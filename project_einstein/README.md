# Project Einstein
___
Type: Algo

By: Ernest Chiu

##Problem

Keith has stumbled upon a problem from an alternate dimension! The problem is here.

Project Einstein Problem 1000401

Let d(k) be the sum of all divisors of k.

We define the function S(n) to be the sum over all 5-tuples of positive integers (i, j, k, l, m) of d(i)\*d(j)\*d(k)\*d(l)\*d(m) where i+j+k+l+m=n.

For example, S(7) =
d(1)\*d(1)\*d(1)\*d(1)\*d(3) + d(1)\*d(1)\*d(1)\*d(3)\*d(1) + d(1)\*d(1)\*d(3)\*d(1)\*d(1) + d(1)\*d(3)\*d(1)\*d(1)\*d(1) + d(3)\*d(1)\*d(1)\*d(1)\*d(1) +
d(1)\*d(1)\*d(1)\*d(2)\*d(2) + d(1)\*d(1)\*d(2)\*d(1)\*d(2) + d(1)\*d(2)\*d(1)\*d(1)\*d(2) + d(2)\*d(1)\*d(1)\*d(1)\*d(2) + d(1)\*d(1)\*d(2)\*d(2)\*d(1) +
d(1)\*d(2)\*d(1)\*d(2)\*d(1) + d(2)\*d(1)\*d(1)\*d(2)\*d(1) + d(1)\*d(2)\*d(2)\*d(1)\*d(1) + d(2)\*d(1)\*d(2)\*d(1)\*d(1) + d(2)\*d(2)\*d\*d(1)\*d(1)
= 110.

You are given that S(10^3) mod 1000000007 = 932450247 and S(10^5) mod 1000000007 = 102356396.

Find S(10^15) mod 1000000007.

##Solution


tl:dr; Eisenstein Series, a theorem of Ramanujan, matrix solving



Project Euler is notorious for requiring more math ability than programming ability. This is especially true for this problem.

Let d\_k(n) be the sum of the kth powers of the divisors of n. Clearly, d\_1(n) = d(n).

The trick is to use certain generating functions for d\_k(n) called [normalized Eisenstein Series](http://en.wikipedia.org/wiki/Eisenstein_series). Specifically, they satisfy

E\_2k = 1 - 4k / B\_2k \* sum d\_(2k-1)(n) \* q^n

where B_k are the Bernouilli Numbers.

E\_2 = 1 - 24 sum d(n)\*q^n

E\_4 = 1 + 240 sum d\_3(n)\*q^n

E\_6 = 1 - 504 sum d\_5(n)\*q^n

E\_8 = 1 + 480 sum d\_7(n)\*q^n

E\_10 = 1 - 264 sum d\_9(n)\*q^n

Furthermore, define the operator θ to be q*(d/dq), where d/dq is the derivative with respect to q.

Ergo,

θE\_2 = -24 sum n\*d(n)\*q^n

θE\_4 = 240 sum n\*d\_3(n)\*q^n

θE\_6 = -504 sum n\*d\_5(n)\*q^n

θE\_8 = 480 sum n\*d\_7(n)\*q^n

A theorem of [Ramanujan](http://en.wikipedia.org/wiki/Srinivasa_Ramanujan) states that θE\_N is a linear combination of E\_2\*E\_N  and S\_(N+2), where S\_(N+2) is a modular form of weight (N+2).
Luckily, for N = {2, 4, 6, 8}, the only modular form of weight N+2 (up to a multiple) is E_(N+2).

We can state this succinctly:

1. E\_2\*E\_N ∈ { θE\_N, E\_(N+2) } for N = {2, 4, 6, 8}.
2. θE\_N ∈ {E\_2\*E\_N , E\_(N+2) } for N = {2, 4, 6, 8}.

where we understand this notation to mean that E\_2 \* E\_N is a linear combination of θE\_N and E\_(N+2), and that θE\_N is a linear combination of E\_2\*E\_N and E\_(N+2).

Furthermore, θ also satisfies the product rule for derivatives (easy to check).

By clever use of the product rule, and the two identities above, we can eventually derive

2. E\_2\*E\_2 ∈ {θE\_2, E\_4}
3. E\_2\*E\_2\*E\_2 ∈ {θθE\_2, θE\_4, E\_6}
4. E\_2\*E\_2\*E\_2\*E\_2 ∈ {θθθE\_2, θθE\_4, θE\_6, E\_8}
5. E\_2\*E\_2\*E\_2\*E\_2\*E\_2 ∈ {θθθθE\_2, θθθE\_4, θθE\_6, θE\_8, E\_10}


Finally, consider (E\_2 - 1)^5. By binomial expansion, we know that (E\_2 - 1)^5 ∈ {1, E\_2, (E\_2)^2, (E\_2)^3, (E\_2)^4, (E\_2)^5}.

But (E\_2 - 1)^5 = (1 - 24 sum d(n)\*q^n - 1)^5 = (-24 sum d(n)\*q^n)^5 = -7962624 \* sum (i+j+k+l+m=n) d(i)\*d(j)\*d(k)\*d(l)\*d(m)\*q^n = -7962624 \* sum S(n)*q^n.

Finally, this implies that S(n) is a linear combination of the nth coefficients of {1, E\_2, θE\_2, θθE\_2, θθθE\_2, θθθθE\_2, E\_4, θE\_4, θθE\_4, θθθE\_4, E\_6, θE\_6, θθE\_6, E\_8, θE\_8, E\_10}.

The coefficients of all of those series require only the factorization of n and no numbers below n.

We use a computer to find the coefficients, by computing the first 16 values of S(n), and solve the matrix equation to find what the linear combination actually is.

We obtain
S(n) = (11 \* d\_9(n) + (-150 \* n + 100) \* d\_7(n) + (720 \* n^2 - 840 \* n + 210) \* d\_5(n) + (-1440 \* n^3 + 2160 \* n^2 - 900 \* n + 100) \* d\_3(n) + (864 \* n^4 - 1440 \* n^3 + 720 \* n^2 - 120 \* n + 5) \* d\_1(n) ) / 331776.

The value of S(10^15) modulo 1000000007 is 227605347.

