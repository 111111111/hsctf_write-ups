#Who Is It?

Type: Recon

By Aaron Berger

##Problem

    public class Problem {
    	public static String a()
    	{
    		double b=0;
    		for (double i=1;i<Double.POSITIVE_INFINITY;i++)
    		{
    			b+=1/i/i;
    		}
    		b=Math.sqrt(b*6);
    		return ""+b;
    	}
    	public static int b()
    	{
    		return a().indexOf("999999");
    	}
    }


##Solution
The first part of this code, String a(), calculates pi.  It is known as the Basel Problem and was proved by Euler.  The second part of the code, int() b, searches pi for when the string “999999” appears in its digits, and this is known as the Feynman Point.  Thus, the inspiration for the code was Feynman, and that is the flag.
