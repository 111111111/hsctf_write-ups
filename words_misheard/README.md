#Words Misheard

Type: Recon

By Aaron Berger

##Problem
Silent! Listen...
I run to escape a persecution.
The eyes, they see.
The flag is insatiable.

##Solution
A quick search of the line “I run to escape a persecution” should reveal that it is an anagram (specifically, “I run to escape” is an anagram of “a persecution”).  This can be confirmed with the other two lines out of the first three.
Finally, you can find that the word “insatiable” has a one-word anagram, namely, **banalities**, which is the flag.
