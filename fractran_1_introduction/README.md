#FRACTRAN 1: Introduction

Type: FRACTRAN

By Aaron Berger

##Intro to FRACTRAN
FRACTRAN is a special type of programming language that relies entirely on math. The code is simply a series of fractions. The user enters an input integer n and the program executes as follows:

1. For the first fraction f in the list for which nf is an integer, replace n by nf.
2. Repeat this rule until no fraction in the list produces an integer when multiplied by n, then halt (Wikipedia).

Clarification: Each time you successfully multiply by a fraction, you go back to the start of the list.

Example: The basic addition program is simply \\(\frac{3}{2}\\). The user inputs the integer \\(2^a \times 3^b\\), and for any a and b, this will return \\(3^{a+b}\\)--try it for yourself. Addition! Yay!

Note that the input and output are encoded. While we say this program takes inputs a and b, the actual input is of the form \\(2^a\times 3^b\\). Similarly, the output is not \\(a+b\\), but rather \\(3^{a+b}\\).

You can learn more by reading the document [States in FRACTRAN](http://compete.hsctf.com/ciJsbu4VFk/States%20in%20FRACTRAN.pdf) we have conveniently provided, using [this document](http://www.cs.vu.nl/~diem/publications/slides/fractran-2011-05-31.pdf) (special thanks to Brett0_Ghett0 for finding it!) or by going to the [Wikipedia article](http://en.wikipedia.org/wiki/FRACTRAN).



##Problem
Write a program that takes input \\(2^a \times 3^b\\)  and returns output \\(5^{a-b}\\) (you can assume \\(a\geq b\\)). Fraction limit: 2.

##Solution 1

The standard subtraction program is \\(\frac{1}{6}\\).  This takes input \\(2^a\times3^b\\) and returns \\(2^{a-b}\\) if \\(a\geq b\\).  Since we’re given \\(a\geq b\\), starting with the fraction \\(\frac{1}{6}\\) is guaranteed to return \\(2^{a-b}\\).  After that, adding the fraction \\(\frac{5}{2}\\) will transfer the \\(a-b\\) from the 2s to the 5s, thus completing the requirement that we needed to end up with  \\(5^{a-b}\\).  Thus the final program is \\(\frac{1}{6},\frac{5}{2}\\).  
**Sample Flag:** 1/6,5/2

##Solution 2
The standard addition program is \\(\frac{x}{y}\\).  In this case, we want to add everything in the 2s to the 5s, so we’ll use the fraction \\(\frac{5}{2}\\).  This will take the input \\(2^a\times 3^b\\) and give us \\(3^b \times 5^a\\).  The standard subtraction program is \\(\frac{1}{xy}\\), so we will use \\(\frac{1}{15}\\) to subtract the 3s from the 5s, thus returning \\(5^{a-b}\\). The final program is then \\(\frac{5}{2},\frac{1}{15}\\) (In this case, \\(\frac{1}{15},\frac{5}{2}\\) would work as well).  
**Sample Flag:** 5/2,1/15
