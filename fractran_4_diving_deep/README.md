# FRACTRAN 4: Diving Deep

Type: FRACTRAN

By Aaron Berger

##Problem

Write a program that takes input \\(2^a\\)  and returns \\(3^{(2^a ) }\\). Fraction limit: 8.

##Solution

This one is going to be pretty similar in concept to a multiplication program.  First we will have an initialization step that initializes the 3s to one.  Then, for each subsequent step, we will decrement the 2s by one and double the 3s.

Let’s write the doubling step first.  For the doubling step, we will use the state makers 7 and 11.  Each step we will decrement the 3s by one and increment the 5s by two, until there are no more 3s left.  Then we will copy all the 5s back over to the 3s, thereby accomplishing the goal of doubling the 3s.  To do this, we will use state markers 7 and 11 for the first part, and 11 and 13 for the second part.  \\((\frac{5^2}{3}\times \frac{11}{7}),\frac{7}{11}\\)  will double the 3s into the 5s, \\(\frac{13}{7}\\) will transition to the next step, and \\((\frac{3}{5}\times\frac{17}{13}),\frac{13}{17}\\) will transfer all the 5s back to the 3s, so we have achieved a net effect of doubling the 3s.  Then, we need to decrease the 2s by one and restart the cycle, which we can do with \\((\frac{1}{2}\times\frac{7}{13})\\).

Next, we need the initialization step, which will initialize the 3s to one and add the state marker for starting the loop.  We also need to make sure this step doesn’t execute once the program is over, so we will check that there’s a 2, otherwise we know we’ve completed all the multiplication. We can use  \\((\frac{1}{2}\times\frac{3}{1}\times\frac{7}{1})\\) to do this.  Note that we start with the state marker 7 instead of 13 because we’ve already decreased the 2s by one, so there’s no need to do it again—we can skip right to the doubling step.

Finally, after everything is done executing, we’ll be left with an extra state marker (13) so we get rid of it with \\(\frac{1}{13}\\).  The final program is: \\(\frac{275}{21},\frac{7}{11},\frac{13}{7},\frac{51}{65},\frac{13}{17},\frac{7}{26},\frac{21}{2},\frac{1}{13}\\).  
**Sample Flag:** 275/21,7/11,13/7,51/65,13/17,7/26,21/2,1/13

