#Number One Fan

Type: Recon

By Aaron Berger

##Problem
\#1
\#2
\#3
\#4
\#5
\#6
\#7
\#8
\#j
\#10


##Solution
The use of #1, #2, and so on through #10 would indicate that rather than be a hashtag or pound symbol, the # represents the word “number”.  A search of “number j” would lead you to StarCraft commentator Sean “Day\[9\]” Plott who at one point in his webshow had a blooper where he said “the number j”, and the fact that he has the number 9 in his (user)name should confirm that you are on the right track.  From here, you take a hint from the problem title, “Number One Fan”, that you might want to look up something to do with Day9 fans.  This part might take a bit longer, but you should eventually (or very quickly, if you happen to search “Day9 fan club”) stumble upon [this forum thread](http://www.teamliquid.net/forum/fan-clubs/130507-day-fan-club), which is a (mostly inactive) Day9 fan thread on one of the most active Starcraft community websites.  Go to the last page (page 173) and you will see a post by user “ceeteef” that says “Flag is Pudding”, and the flag is indeed “Pudding”.
Shoutout to jackv from team "Faded Gray" for being the only one to solve this problem during the competition!
