#Chips In The Middle

Type: Algo

By Aaron Berger

##Problem
/ley, #atha, and Keith (pronounced slashley, hashtagatha, and keith) are playing a game. /ley starts by putting 1 chip in the middle of the table (turn 1). #atha puts 2 chips in (turn 2). Keith puts 3 in (turn 3). /ley puts 4 in (turn 4). They continue this (putting in n chips on turn n) until there are 1326 chips in the middle, at which point, for some inexplicable reason, #atha is declared the winner. How many turns did this take?

##Solution 1
We can write a simple code to keep a running total of how many chips are in the middle on each turn.  In Java, it looks like this:

    public static void chips() {
		int limit = 1326;
		int runningTotal = 0;
		int turn = 0;

		while (runningTotal < limit) {
			turn++;
			runningTotal += turn;
			System.out.println("TURN " + turn + "  CHIPS: " + runningTotal);
		}
	}

This code, when executed, tells us that there are 1326 chips in the middle on turn 51, so the flag is 51.

##Solution 2:
You might know that formula for the sum of the integers from 1 to n is n*(n+1)/2.  We set this equal to 1326 and solve for n, either by hand or by using a tool like [Wolfram|Alpha](http://www.wolframalpha.com/input/?i=n%28n%2B1%29%2F2%3D1326%2C+solve+for+n), which gives the positive answer of 51.
