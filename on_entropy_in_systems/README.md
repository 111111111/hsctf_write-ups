#On Entropy In Systems

Type: Recon

By Aaron Berger

##Problem
![On Entropy In Systems Picture](http://compete.hsctf.com/ciJsbu4VFk/numberbox.jpg)

##Solution
First, you will need to find OEIS, the Online Encyclopedia of Integer Sequences.  There are many ways to find it if you didn't know what it was; for example, a Google search of the 5th sequence, [if done correctly like this](https://www.google.com/#q=%222%2C+1%2C+2%2C+4%2C+10%2C+37%2C+138%2C+628%22) will lead you there.  Note that the problem title also has the acronym (or initialism, to be precise) OEIS as confirmation that you're on the right track.  All 7 rows are parts of sequences in OEIS, and their sequence numbers  are 1, 4, 13, 36, 87, 190, 386.  This is also a sequence in OEIS (sequence 2727), and its 8th term is 734, so we know the 8th row of the box will correspond to sequence 734 in OEIS.  This sequence is 1, 2, 5, 15, 49, 177, 715, 3255, and the red box, which corresponds to the 8th term, would be 3255, which is the flag.
