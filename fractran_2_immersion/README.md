#FRACTRAN 2: Immersion

Type: FRACTRAN

By Aaron Berger

##Problem
Write a program that takes input \\(2^a\\) and returns output \\(3^{a+1}\\).  Fraction limit: 3.

##Solution
The method we will use to solve the problem is as follows:
* If there are at least two 2s, remove one of them and add one to the 3s.
* 	Otherwise, when there’s exactly one 2 left, remove it and add two to the 3s.

The first step will be written as \\(\frac{2}{5},\frac{15}{4}\\)  .  If there’s at least two 2s, then the \\(\frac{15}{4}\\) will execute, decreasing the 2s by two and increasing each of the 3s and the 5s by one. Then the \\(\frac{2}{5}\\) will execute, getting rid of the 5 and adding one back to the 2s.  The net effect of these two steps is a decrease of one in the 2s, and an increase of 1 in the 3s, however this will only work as long as there are two 2s.
Next, we use the fraction \\(\frac{9}{2}\\) to get rid of the last 2 and increase the 3s by two, which will guarantee that we end up with one more 3 than the number of 2s we started with.  Therefore, the final program is \\(\frac{2}{5},\frac{15}{4},\frac{9}{2}\\).  
**Sample Flag:** 2/5,15/4,9/2
