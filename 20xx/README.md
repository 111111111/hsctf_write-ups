# 20XX
___
Type: Crypto

By: Ernest Chiu

##Problem

The year is 20XX. P = NP, and the entire complexity hierarchy has collapsed.
Society has split into the have and have-nots-- the people who can efficiently devise paths for salesman and those who CANNOT.

Unfortunately, Keith is just a simple time traveler who is stuck here after a catastrophic ***REDACTED***
Can you help him break a cryptographic system based on a NP-complete problem, or will Keith be a have-not forever???

The source is here, and the public key is here. The ciphertext is 369096909670720443552302608769100062.


##Solution


TL:DR; weak key generation

There were three approaches to the problem.

1. Brute Force or Dynamic Programming:
Too long, requires O(\\(2^N\\)) or O(\\(2^(N/2)\\)) time.
2. Merkle Hellman 'Break':
People looked up the name of the cipher, and saw that Merkle Hellman is broken.
Unfortunately, it takes O(\\(N^10\\)), still too long.
3. LLL algorithm:
People tried to find short vectors in the lattice spanned by the public key and the ciphertext.
LLL only gives guaranteed results in the low density case, so this doesn't work even most of the time.

The break is the weak key generation.
Specifically, the lines

	new = sums+randint(1,10)

	if (is_probable_prime(i))


Let \\(X_{i}\\) be an integer between 1 and 10.

Then we know the private key satisfies:

\\(a_1 = a_1\\)

\\(a_2 = X_2 + a_1\\)

\\(a_3 = X_3 + \sum\limits_{i=1}^2 a_i\\)

.
.
.

\\(a_N = X_N + \sum\limits_{i=1}^{N-1} i^2 \\)

So the public key satisfies:

\\(b_1 \equiv r\*a_1 \bmod{q}\\)

\\(b_2 \equiv r\*a_2 \equiv r\*X_2 + r\*a_1 \bmod{q} \\)

\\(b_3 \equiv r\*a_3 \equiv r\*X_3 + \sum\limits_{i=1}^{2} r\*a_i \bmod{q}\\)

.
.
.

\\(b_N \equiv r\*a_N \equiv r\*X_N + \sum\limits_{i=1}^{N-1} r\*a_i \bmod{q} \\)

This gives us an idea. Why not isolate the \\(X_i\\), which are between 1 and 10, then find a collision to solve for q?

Explicitly, define \\(c_i\\) to be \\(b_i - \sum\limits_{j=1}^{i-1} b_j\\).

Then we get

\\(c_2 \equiv b_2-b_1 \equiv r\*X_2 \bmod{q}\\)

\\(c_3 \equiv b_3-\sum\limits_{i=1}^{2} b_i \equiv r\*X_3 \bmod{q}\\)

.
.
.

\\(c_N \equiv b_N-\sum\limits_{i=1}^{N-1}b_i \equiv r\*X_N \bmod{q}.\\)

If we consider any 11 \\(X_i\\), we know that at least two must have the same value.
(since they are integers from 1 to 10).

Of those two, if we subtracted their corresponding \\(c_i\\) from each other, we would obtain a number \\(0 \bmod{q}\\).
Since we know q is larger than all of the ciphertexts, it must be an extremely large prime factor. \\(>2^100\\)

Ergo, we take the pairwise differences of {\\(c_2, c_3,..., c_12\\)}, factor them, and output any large factor.
Factoring takes a while, so I recommend using a library or wolframalpha.

Eventually, we get that q = 22039236155546818188884293061349511.

We can find r by allowing \\(X_2\\) to range from (1,10), solving for r in the equation for \\(c_2\\), and testing whether it creates an easy knapsack.

We find that r = 18899218394841835034678545355596444.

Finally we can solve by inverting the knapsack, and the flag is N@pP0u(h*B3@7.
