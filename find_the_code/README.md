# Find The Code

Type: Recon

By Aaron Berger
___________

##Problem
![Find The Code Image](http://compete.hsctf.com/ciJsbu4VFk/findthecode.jpeg)

##Solution

A quick reverse image search will reveal that this is a picture of Norman Joseph Woodland, famously one of the inventors of the barcode.  Along the bottom of the image is a series of black and white marks, which one might reasonably guess is therefore a barcode.
After enlarging those marks, it looks like this:
![Find The Code Barcode](http://compete.hsctf.com/ciJsbu4VFk/raisethebar.png)
Which certainly seems barcode-like.
A quick scan on an online barcode reader (For example, [this one](http://online-barcode-reader.inliteresearch.com/)), reveals that yes, it is a valid barcode, and it translates to “Raise The Bar”, which is the flag.


