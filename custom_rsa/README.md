# Custom RSA
___
Type: Crypto

By: Ernest Chiu

##Problem
Break the RSA encryption used by Keith the super spy to hide his dark secrets!
##Solution
tl:dr; small private key attack

Specifically, the problem is the line

    long d = reader.nextLong();

and the lines

    BigInteger p = BigInteger.probablePrime(256, r);

    BigInteger q = BigInteger.probablePrime(256, r);

There are so few valid attacks on RSA, that a simple google search reveals [Dan Boneh's survey of RSA attacks](http://crypto.stanford.edu/~dabo/papers/RSA-survey.pdf).

The attack is the low private key exponent attack. That is, p and q are the same bit size, so either q<= p <=2q or the other is true. Also, the private key fits in a long, which is an eighth of the size of the modulus.

We calculate the partial fractions of e/N, test the denominator of truncated fractions for the private key.

Finally we obtain the **flag:** smallprivatekeymakesmecry
