#Keith the Urban Planner

Type: Algorithm
By Alok Tripathy

##Problem

The problem statement is [here](http://compete.hsctf.com/ciJsbu4VFk/KeithTheUrbanPlanner.txt).
Here are the necessary text files:
[Urban Planner](http://compete.hsctf.com/ciJsbu4VFk/UrbanPlanner.txt).
[Prices](http://compete.hsctf.com/ciJsbu4VFk/Prices.txt).

##Solution

You're given a graph G = (V, E). To find the lowest possible price one has to pay for the roads, one has to find the fewest number of roads needed to be crossed to go from the School to the House of Edels (which, by the way, is a play on the name "Edelman". We did most of the work at their house). This can be done with a [BFS](http://en.wikipedia.org/wiki/Breadth-first_search) (some people said they used [Dijkstra](http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) with all edge weights equalling 1, which also works). The fewest number of roads that connects these two buildings is 8. Now that we know the fewest number of roads necessary, all we need is the 8 smallest road prices. This can be done by sorting the array of prices and taking the first 8 elements. The sum of these elements is the answer, **480**.

Here is some Java code that does [this](https://dl.dropboxusercontent.com/u/60294520/KeithTheUrbanPlanner.java)