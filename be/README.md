# be
Type: Recon

By Ben Edelman

##Problem
My name is Ben Edelman.

##Solution
Yes, my name is Ben Edelman, as you may have gathered by now. Much of the difficulty of this recon challenge lies in the fact that there is [another Ben Edelman](http://www.benedelman.org/) who dominates the first page of results on [DuckDuckGo](https://duckduckgo.com/?q=ben+edelman) or any other [search](https://www.google.com/#q=ben+edelman) [engine](http://www.bing.com/search?q=ben+edelman). Of course, the flag from this problem was posted by *me*, so you must find me on the Internet.

You can note that I go to school at West Windsor-Plainsboro High School North; however, the flag doesn't lie along that path. The key insight is that I am an organizer of HSCTF (surprise!) and thus follow the [HSCTF Twitter account](https://twitter.com/hsctf) on Twitter. At the time of the competition, I had posted a grand total of one Tweet:


> Verifying myself: I am be on Keybase.io. d2P-GPBqIa8jRcH_saX2JYDB-6Oa1LGBQsgw / [https://keybase.io/be/sigs/d2P-GPBqIa8jRcH_saX2JYDB-6Oa1LGBQsgw …](https://keybase.io/be/sigs/d2P-GPBqIa8jRcH_saX2JYDB-6Oa1LGBQsgw)

This Tweet is used by Keybase to verify that my Keybase account was owned by the same person as my Twitter account at the time of tweeting.

Let's look at that [Keybase](https://keybase.io/) account! It has my public key, which was not gamed for the purpose of this competition. But it also contains a user profile with some strange text:

> Hive mind. Artichoke. Casual zombie. What type of base is this? cGljb2N0Zl9pc19jb250cm9sbGVkX2 J5X3RoZV9uc2E=

> 39°06'34.2"N 76°46'22.7"W

Hmmm...The coordinates give the location of the NSA. BINGO! Actually, that's a red herring.

What does "What type of base is this?" mean? Well, Keybase is a 'key' base. So the key might be nearby! What about that strange string:

> cGljb2N0Zl9pc19jb250cm9sbGVkX2 J5X3RoZV9uc2E=

Well, the question "What type of base is this?" is useful again. This string is ASCII encoded in base 64. Converting to plain text reveals the flag.

**Flag:** picoctf_is_controlled_by_the_nsa
