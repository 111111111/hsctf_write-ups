# HSCTF Write-ups

##Wow.
This was fun. Running a CTF was an exhilarating, amazing experience, and from what we gather, so was participating. The idea for HSCTF germinated after we (most of the organizers) participated in picoCTF and CSAW CTF Quals. We enjoyed both competitions tremendously, but were dismayed to find that there were very few entry-level CTFs, and no high school CTFs in the 2013-14 academic year. This year, we started a computer science club in our school, and ran a small in-school beginner CTF. It was a huge success. We knew we wanted to make something bigger. We also knew we didn't want our CTF to be purely about security. HSCTF shows that people can learn any aspect of computer science from a CTF.

We began planning for a CTF on March 18, 2014. All the problems were written in the month before HSCTF began. We were successful in fine-tuning a working website based off of [picoCTF's CTF Platform](https://github.com/picoCTF/CTF-Platform).

We expected about 30 teams to join our CTF. We fantasized about getting 100 teams. The fact that **over 750 teams with over 2000 students** submitted correct flags is *insane*. Competitors came from more than two-thirds of states in the USA, in addition to many other countries including Belgium, Switzerland, South Korea, and even Ukraine.

##Acknowledgements
We would like to thank our excellent sponsors, Trail of Bits, Facebook, and Educoin, for making HSCTF possible. We would like to give a special thanks to Trail of Bits for giving us the idea to do the writeups in the form of a GitBook because of their excellent [CTF GitBook](https://trailofbits.github.io/ctf/). Also a huge help was the [Computer Science Teachers' Association](http://csta.acm.org/), which got the word out about our competition. We'd also like to thank the amazing organizers of [picoCTF](https://picoctf.com/) for helping us use their platform and aiding us in setting up HSCTF in many other ways.

Thanks to our faithful problem testers: Ezra Edelman, Timothy McFarlane, Nikhil Phatak, Sammy Berger, and Eric Yang.

Thank you to the teachers across the country who organized their students and encouraged them to participate in HSCTF. 75% of students said they heard about HSCTF from their teachers.

Finally, thank you to the participants in HSCTF! You worked extremely hard, dedicated a week of your life to HSCTF, entertained us on the IRC, and solved some very difficult problems.

##Going Forward
Due to the success of HSCTF 2014, we are delighted to announce HSCTF 2015, which will take place in (you guessed it) 2015. We will improve the competition in many ways based on your feedback, and will make it a more polished product overall. If you have any recommendations on how we can make HSCTF a better experience, please contact Jacob at [jacob@HSCTF.com](mailto:jacob@HSCTF/com).

Thank you so much!

Sincerely,
The organizers of HSCTF  
**Jacob Edelman:** President, Outreach Warlock

**Alok Tripathy:** Vice President, Algorithm Ninja

**Aaron Berger:** Primary Problem Writing Guru

**Ernest Chiu:** Math Lord

**Ben Edelman:** Web Development Master, Design Mage

**Aaron Weiss:** Haskell Unicorn
