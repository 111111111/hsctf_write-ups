# 100 Point Problems
The aimed level of difficulty for 100 point problems was to be accessible to everybody, even newcomers to the world of computer science. They are aimed at helping you understand the basics and get a good idea of what tools could be used to approach the problems.
