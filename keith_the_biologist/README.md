#Keith the Biologist

Type: Algorithm

By: Alok Tripathy

##Problem
The problem statement is [here](compete.hsctf.com/ciJsbu4VFk/KeithTheBiologist.txt). The case is [here](http://compete.hsctf.com/ciJsbu4VFk/Biologist.txt).


##Pre-Solution Talk

Okay, so, this problem (as you might have been able to tell) didn't turn out quite the way I wanted it to. There were a few solutions I tried to make
infeasible that turned out to be okay on this case. I'll discuss both the intended O(V+E) solution and the solution that worked O(V(V+E)). If you want to work on
the O(V+E) solution for enjoyment, you should stop reading here and work on it. If not, continue :D

##Solution 1 - The Solution that Worked

This was the first solution that came to me when working on it, and it's pretty easy to see how this works. 

Pick a vertex. Any vertex. Name it v. Our first goal is to find out what vertices v can get to. In other words, we're trying to find all the vertices v can reach
and put all these vertices in a set. To do this, run a [depth-first search](http://en.wikipedia.org/wiki/Depth-first_search) on this vertex and insert all 
visited vertices into this set (I call this set the connectivity set). Do the same thing for every vertex. Now you know which vertices connect to which vertices. 
With this information, we only have to find two vertices A and B such that B is not in A's connectivity set and B is not in A's connectivity set. This can
be done simply by checking every pair of vertices A and B and their corresponding connectivity sets.

##Solution 2 - The Intended Solution

This was more of what I was looking for, but does require some prior knowledge of graph theory algorithms and concepts. This algorithm runs in O(V+E) time.

Find the [strongly connected components (SCCs)](http://en.wikipedia.org/wiki/Strongly_connected_compodsdfnents) of the given graph G. This can be done using the
[Kosaraju-Sharir](http://en.wikipedia.org/wiki/Kosaraju%27s_algorithm) algorithm in O(V+E) time. Now, form a new graph G^SCC = (V', E'), the strongly-connected 
component graph, by representing each SCC with a vertex and edges between SCCs with edges. Since SCCs are defingued as subgraphs where every vertex is reachable
from every other vertex, if we can find two vertices in G^SCC that are not reachable from each other. This can be done by finding a topological sort of G^SCC,
which can be done in O(V'+E') time using a DFS. Since |V'| <= |V| and |E'| <= |E|, the overall time complexity of O(V+E) remains the same. After we find a 
topological sort of this graph, look at every pair of consecutive vertices i and i + 1 in the t-sort of G^SCC. If there is no edge from i to i + 1, there is no
path from any vertex in the ith SCC and the (i+1)th SCC. Thus, any vertex from SCC i and any vertex from SCC i + 1 could be an answer.

Here is some Java code that does [this](https://dl.dropboxusercontent.com/u/60294520/KeithTheBiologist.java).
