#Just Learning

Type: Reversing

By Alok Tripathy

##Problem
Keith the Knight is learning how to program, but he is having trouble finding out the flag for his Intro to CS assignment. Help him out by doing it for him. Here is his assignment:

    public class JustLearning {

        public JustLearning() {
            char[] letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K','L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

            System.out.println("Key is: " + new String(letters));
        }

        public static void main(String[] args) {
            new JustLearning();
        }
    }

##Solution
Creating a new String (new String(letters)) concatenates all the characters in the character array letters. This gives you the string **ABCDEFGHIJKLMNOPQRSTUVWXYZ**, which is the flag.
