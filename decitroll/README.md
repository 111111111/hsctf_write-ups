#Decitroll

Type: Crypto

By Aaron Berger

##Problem
BED97 \* CAF8DD = C96DD777AB
DA9 \* DDD667 = ???

##Solution
After a bit of testing, you can find that the multiplication in the first line works properly if you replace the letters with their corresponding numbers, starting with A=0 through F=5. After doing the same with the second line, the product comes out to be 103103103, which, when converted back to the corresponding letters, spells out BADBADBAD, which is the flag.
