# Keith Numbers 1

Type: Algo

By Aaron Berger

##Problem
Oh no! A group of aliens (all named Keith) have taken over the world. They are especially interested in numbers called Keith Numbers, which they have defined as follows:

1. Take a positive integer x. Let the length (number of digits) of x be n (n can be 1).
2. Create a new Fibonacci-like sequence where the first n terms are the digits of x in the order they appear (from left to right) and each successive term in the sequence is the sum of the previous n terms.
3. If x appears in this sequence, it is a Keith number.

For example, 197 is a Keith Number because the sequence it forms is 1,9,7,17,33,57,107,197.

The aliens have now created a game. Every human has to pick a number to use as a base (from 2 to 36). The aliens will then count how many Keith Numbers in that base have a base 10 value less than 500000. Finally, they will kill all the humans who didn't pick the base with the highest count.

As a computer programmer, you need to save the human race! Find the base between 2 and 36 with the highest count of Keith Numbers with base 10 value less than 500000 so you can inform and save everyone else before it's too late.

Solution format: (base, number of Keith numbers for that base)
Example input: (2,29)

##Solution
The program we're going to write will work as follows:
For each base:
For each number from 1 to 500,000:
1. Convert the number to the base.
2. Generate the starting numbers of the sequence.
3. Convert these starting numbers back to base 10 (note that once we have the correct starting numbers, it doesn't matter which base we generate the sequence in, so we might as well convert it back to base 10 to make our lives easier).
4. Generate each term in the sequence by adding the previous n terms (here I will use an optimization involving a running total to make this a bit faster).
5. Check if the number appears in its own sequence
6. Count the total number of Keith numbers in the base, and find the base with the highest amount.

Here's my code (in Java) for performing this task.

    public static int countNums(int base, int limit) {
		int counter = 0;
		for (int i = 1; i < limit; i++) {
			//Step 1
			String gen = Integer.toString(i, base);

			//Step 2
			ArrayList<Integer> baseN = new ArrayList<Integer>();
			for (int digit = 0; digit < gen.length(); digit++) {
				//Step 3
				baseN.add(Integer.parseInt(gen.substring(digit, digit + 1),
						base));
			}

			//Step 4
			ArrayList<Integer> sequence = new ArrayList<Integer>();
			sequence.add(baseN.get(0));

			int total = baseN.get(0);
			while (sequence.get(sequence.size() - 1) < i) {
				if (sequence.size() < baseN.size()) {
					sequence.add(baseN.get(sequence.size()));
					total += sequence.get(sequence.size() - 1);
				} else {
					sequence.add(total);
					total -= sequence.get(sequence.size() - baseN.size() - 1);
					total += sequence.get(sequence.size() - 1);

				}
			}

			//Step 5
			if (sequence.get(sequence.size() - 1) == i) {
				counter++;
			}
		}
		return counter;
	}

	//Step 6
	public static void main(String[] args) {
		int max = 0;
		int maxBase = 0;

		for (int base = 2; base <= 36; base++) {
			int n = countNums(base, 500000);
			System.out.println("Base " + base + "  Amount: " + n);

			if (n > max) {
				max = n;
				maxBase = base;
			}
		}
		System.out.println("---------");
		System.out.println("Max Base: " + maxBase + "  Amount: " + max);
	}

This will tell you that the base with the most is 33 with a total of 114 Keith numbers, so the flag is (33,114).

###Comment:
Since the entire competition made references to the name Keith, I'm told by multiple people that they didn't think to look up Keith Numbers online.
I didn't invent Keith numbers, however; [they are an actual mathematical concept](https://en.wikipedia.org/wiki/Keith_number) and there is a bit of documentation (though not much) about them.
