# 8 Funky Yellow Ducks
By Jacob Edelman

Type: Recon

##Problem
Note that this problem was changed to make the adjective more common.
Sadly the admin to your cat picture database, Keith, has lost his password.
What he does remember is that it is in the form of HSCTF[number from 0 to 9 inclusive][common adjective][a common color][the plural form of the name of a common animal].

All letters except the inital HSCTF(which is uppercase) are lowercase.
For instance it might be HSCTF8funkyyellowducks.

Keith also knows the md5 hash of his password is the hexadecimal string 0c283432b2ef5adea008bcd7c27f4c3f, so you will have to brute force the correct password.

Find the Keith's password.

##Solution
A bit of backstory on this problem: This password system was desgined to be identical to the one used by our school. It was only during the competition that I learned that our school only had ~8 possible adjectives, animals, and colors, and so even this weak and easily breakable password system is better than the one my school uses.

The intended solution is to simply brute-force this problem using your favorite programming language. The hardest part by far about this problem is finding a suitable list of words. For the colors, even a small list of basic colors would work. For adjectives, any of the first few results of googling "common adjective list" would do. For a list of animals, a list of common animals could be used from Wikipedia. But, some formating would be needed to get just the animal names, and an "s" would have to be added on to all of them to make them plural. Brute-forcing with this simple dictionary would reveal the flag.

**Flag:** HSCTF1calmbluekangaroos
