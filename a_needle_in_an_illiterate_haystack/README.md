# A Needle in an Illiterate Haystack

Type: Algo

By Jacob Edelman

##Problem
Your quarry, an escaped tangerine named Keith, has mutated into a needle and hidden in a haystack.
Of course, this would normally be easy for you, but sadly, this haystack is illiterate.
Because of this you need to sort through the haystack for the flag.
[Here's the file](http://compete.hsctf.com/ciJsbu4VFk/randomGenProb2.txt) (read the first bit to get started).

##Solution 1
To solve this problem, it is a good idea to split up the code into pieces, and then check those pieces to see if they had a large number of words of sufficient length.  If they did, I would print them out and check manually whether the flag was obviously in one of them.
To do this, I used [this list](https://www.dropbox.com/s/ecphm9zupmhnewe/words.txt) of 1500 or so of the most common English words.  Here's the code:

    import java.io.BufferedReader;
    import java.io.FileReader;
    import java.io.IOException;
    
    public class Reader {
    
    	public static void main(String[] args) {
    		int length = 100;
    		BufferedReader br = null;
    		char[] current = new char[length];
    		char[] previous = new char[length];
    		
    		try {
    			br = new BufferedReader(new FileReader("randomGenProb2.txt"));
    			br.read(current);
    			String s = "";
    			while (!new String(current).equals(new String(previous))) {
    				BufferedReader br1 = new BufferedReader(new FileReader("words.txt"));
    				int counter = 0;
    				while ((s = br1.readLine()) != null)
    					if (s.length() > 3 && new String(current).contains(s))
    						counter++;
    				if (counter > 5)
    					System.out.println(new String(current));
    				previous = current.clone();
    				br.read(current);
    			}
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				if (br != null)
    					br.close();
    			} catch (IOException ex) {
    				ex.printStackTrace();
    			}
    		}
    	}
    }


This code only returns two strings of letters, one of which contains: **theselettersrepresenttheclothmarkerthatunlocksrewards**, which is the flag.

##Solution 2
After solving 1000 Code Monkeys, you might notice that the language is modified so as not to use any words like flag or key.  You might guess that this flag is written in the same style, and search the text file for some of the less common words that appear in its flag.  As it turns out, the word "unlock", which appeared in the previous flag, only appears once in the entire document, and that will give you this flag as well.

