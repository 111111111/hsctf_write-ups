
# Sleepless Soccer Balls
___
Type: Algo

By: Ernest Chiu

##Problem
Keith the factory manager manufactures perfectly identical soccer balls. 
Unfortunately, they are so perfectly identical that Keith cannot go to sleep at night.

To remedy this problem, he places identical red stickers on some of the edges of the soccer ball, so that each face of the soccer ball has exactly one edge with a red sticker.

But now a new problem keeps Keith awake at night--how many soccer balls can Keith make before he makes two identical soccer balls again?

Soccer balls are regular polyhedra with 12 pentagonal and 20 hexagonal faces.
Two soccer balls are identical if they can be rotated so that the stickers are on the same edges.
Faces share edges.

Find the maximum number of different soccer balls.

##Solutions
tl:dr; FKT, Burnside's, multi-edges and self-loops

This problem is very, very frustrating.

First, let's consider what the problem is asking. If every face has exactly one edge with a red sticker, then since there are 32 faces, there are exactly 16 stickers. Furthermore, each arrangement of stickers correspond to matching all faces into adjacent pairs. In computer science, this called a [perfect matching](http://en.wikipedia.org/wiki/Perfect_matching).

Second, we need to count the number of perfect matchings, not up to rotation. The only known algorithm for counting perfect matchings in polynomial time is the [FKT algorithm](http://en.wikipedia.org/wiki/FKT_algorithm).

Third, we need to count the number of perfect matchings, up to rotation. To deal with the symmetry, we use [Burnside's Lemma](http://en.wikipedia.org/wiki/Burnside%27s_Lemma). In layman's terms, we add the number of self-symmetric perfect matchings to the total number of perfect matchings, and divide by the number of symmetries to find the true number of perfect matchings up to symmetry.

Fourth, we need to find the number of self-symmetric matchings. If we look at the possible rotations, we realize only 15 (those that are 180 rotations about an edge between two hexagons) can fix a perfect matching. (The others are rotations centered on a face.) If a matching is self-symmetric, then the image of matched pair of faces under a 180 rotation must also be a matched pair! So it suffices to identify each vertex with its image, and calculate the number of perfect matchings in this new graph.

Fifth, this is a very important subtlety. The new graph has self-loops, corresponding to the edge that connects the two hexagonal faces being rotated about, and multi-edges, corresponding to the two edges of the pentagon adjacent to the two hexagonal faces. The only way to deal with self-loops is to run the FKT algorithm twice.

Finally, assuming the implementation of FKT is correct, we can calculate the total number of matchings to be 562500, the total number of self-similar matchings to be 1168, and the total number of matchings up to rotation to be (562500 + 15\*(1168)) / 60 = 9667 different soccer balls.

Shout out to 0x90.avi for being the only team to solve this problem!