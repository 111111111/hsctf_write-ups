# FRACTRAN 3: The Plunge

Type: FRACTRAN

By Aaron Berger

##Problem

Write a program that takes input \\(2^a\times 3^b\\)  and returns output \\(5^{(2a+1)b}\\). Fraction limit: 6.

##Solution

We can find the basic 6-fraction multiplication program on Wikipedia: \\(\frac{455}{33},\frac{11}{13},\frac{1}{11},\frac{3}{7},\frac{11}{2},\frac{1}{3}\\).  This program takes input \\(2^a\times3^b\\) and returns output \\(5^{a\times b}\\).  We just have to make a few edits to make it do what we want.  First of all, note that \\(455/33\\) is the only fraction where we multiply by 5.  If we multiply the 455 by 5 to get 2275, we will now increment the 5s by two every time we would have incremented them by one.  So the program \\(\frac{2275}{33},\frac{11}{13},\frac{1}{11},\frac{3}{7},\frac{11}{2},\frac{1}{3}\\) will return \\(5^{2ab}\\).  Now we’re almost there!  Next, after some testing about how the program works, we figure out that without the \\(\frac{1}{3}\\) at the end, the program would return \\(3^b 5^{2ab}\\).  So this term of \\(\frac{1}{3}\\) removes this \\(3^b\\).  If instead of \\(\frac{1}{3}\\), we used \\(\frac{5}{3}\\), this \\(3^b\\) would be transferred over to the 5s, thus outputting \\(5^{2ab+b}\\) which is the same as \\(5^{(2a+1)b}\\).  The final program is therefore: \\(\frac{2275}{33},\frac{11}{13},\frac{1}{11},\frac{3}{7},\frac{11}{2},\frac{5}{3}\\).  
**Sample Flag:** 2275/33,11/13,1/11,3/7,11/2,5/3.
