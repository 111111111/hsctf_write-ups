# My name is

Sadly it turned out that a pastebin like service, hastebin, that we used deletes posted content randomly without warning. This meant that the problem became impossible sometime during the competition. We are deeply sorry for this mistake but would like to mention that inputted flags made it appear that nobody even got to the point where this error would have impeded them.
