#Permission Denied

Type: Recon

By Aaron Berger

##Problem
1mr0duscAqQa4E2bFZZ17ZIi_nwF19cQ1pEJJWw1Wmuo/edit?usp=none


##Solution
A few Google searches (or just recognizing it) reveals that the “edit?usp” text is used in Google Drive URLs.  That would lead you to guess that this is part of the link to a Google Doc, and indeed, if you add in the rest of the url, “https://docs.google.com/document/d/” you will get the entire link, which is https://docs.google.com/document/d/1mr0duscAqQa4E2bFZZ17ZIi_nwF19cQ1pEJJWw1Wmuo/edit?usp=none.
Pasting this into a web browser will take you to a document that says **eat\_a\_cookie**, which is the flag.
