# Obfuscated Python

Type: Reversing

By: Jacob Edelman

##Problem

Find the string that causes True to be printed out in <a href=http://compete.hsctf.com/ciJsbu4VFk/obfuscated.py>this program</a>.

##Solution

At first, almost the entire script appears to be insane. The confusion present in the file can be avoided upon taking notice of this line:
```python
hi=eval(y(u(hex(c)[1+1:1-1-1])))
```
In that line a piece of code, namely `y(u(...))`, is executed. By changing the `eval` to a `print` and removing the `hi=` part we can find out what that piece of code is. The outupt is: `lambda x:x=='this_is_some_weird_looking_python'` which is a function checking to see if the input equals `'this_is_some_weird_looking_python'`. That is the flag.

**Flag:** this_is_some_weird_looking_python
