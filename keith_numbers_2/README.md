# Keith Numbers 2

Type: Algo

By Aaron Berger


##Problem
Oh no! After correctly solving the Keith Numbers problem for the aliens, you find out the translation was faulty. You are given a revised version of the problem to solve. Everything that changed is marked in **bold**.

1. Take a positive integer x. Let the length (number of digits) of x be n (n **CANNOT** be 1).
2. Create a new Fibonacci-like sequence where the first n terms are the digits of x in the order they appear (from left to right) and each successive term in the sequence is the sum of the previous n terms.
3. If x appears in this sequence, it is a Keith number.

The aliens have now created a game. Every human has to pick a number to use as a base (from 2 to **36000**). The aliens will then count how many Keith Numbers in that base have a base 10 value less than **50000000**. Finally, they will kill all the humans who didn't pick the base with the highest count.
As a computer programmer, you need to save the human race! Find the base between 2 and **36000** with the highest count of Keith Numbers with base 10 value less than **50000000** so you can inform and save everyone else before it's too late.

Solution format: (base,number of Keith numbers for that base)
Example input: (2,29)

##Solution 0 (Unintended)
A few teams were able to do a GPU-assisted brute force of all the bases, despite the increased limits.  See below for the intended solution.

##Solution 1

Since we're not going to be able to brute force with these increased limits, we want to do an efficient search.  To do this, we will look for where we can expect to find the maximum number of Keith Numbers.  To do this, we will use the smaller limit of 500,000 and search upward through the bases, printing out each time we find a new maximum, until we find a pattern.
To do this, we will need to make a few minor edits to the code from Keith Numbers 1:

**Change 1:** Change the counting method to make sure we don't count any 1-digit Keith Numbers.

    for (int i = base; i < limit; i++) {

**Change 2:** A change in the counting method in the way we generate the first n digits of the sequence (so we can go past base 36):


    ArrayList<Integer> baseN = new ArrayList<Integer>();
		int j = i;
		while (j > 0) {
			baseN.add(0, j % base);
			j /= base;
		}


**Change 3:** A change in the main to only output bases that reach a new maximum, and continue past 36


	for (int base = 2; base <= 300; base++) {
			int n = countNums(base, 500000);
			if (n > max) {
				System.out.println("Base " + base + "  Amount: " + n);
				max = n;
				maxBase = base;
			}
		}

The bases that are the new maximum are 2, 3, 7, 8, 9, 12, 20, 21, 28, 32, 33, 54, 56, 87, 88, 89, 142, 143, 144, 231, 232, and so on.  It's a pretty good guess from this that the maxima will occur right around the Fibonacci numbers.  We then evaluate with the actual limit at numbers close to the Fibonacci numbers, which doesn't take **too** much time, and get that the maximum occurs at 6765 with a total of 6893 numbers, so the solution is (6765,6893).

###Comment
Since the entire competition made references to the name Keith, I'm told by multiple people that they didn't think to look up Keith Numbers online.
I didn't invent Keith numbers, however; [they are an actual mathematical concept](https://en.wikipedia.org/wiki/Keith_number) and there is a bit of documentation (though not much) about them.
