#Asseragod

Type: Recon

By Aaron Berger

##Problem
What city did Asseragods' spouses rule for about 120 years around the turn of the millenium?


##Solution
A search of the word “Asseragod” reveals that backwards, it is “Dogaressa”.  Searching “Dogaressa” should lead you to the Italian Doge.  If you go to the Wikipedia article on the Doge, you can find at the bottom of the page (in the "See also" section) a link to a page on the Doge of Amalfi. Independent dukes (or dogi, as they were known) ruled this city for 115 years, from 957 to 1073, which is right around the turn of the first millennium. Therefore, the correct answer is Amalfi.
