# FRACTRAN 6: Water-Breathing

Type: FRACTRAN/Algo

By Aaron Berger

##Problem

After reading about [Knuth's Up-Arrow Notation](http://en.wikipedia.org/w/index.php?title=Up_arrow_notation), Keith has decided he wants to implement it in FRACTRAN.
He wrote a program that you can find [here](http://compete.hsctf.com/ciJsbu4VFk/UpArrowProgram.txt) as a list of space-separated integers, [here](http://compete.hsctf.com/ciJsbu4VFk/UpArrow2.txt) if you like the standard format better, or in the **Appendix** in standard Java array format, that will take input \\(2^a\times3^n\times5^b\\) and output \\(5^{a (\uparrow^{n-1}) b}\\).

For example, a single up-arrow represents exponentiation, so to calculate \\(6\uparrow4= 6^4\\) he would input \\(2^6\times3^2\times5^4= 360000\\).
A double up-arrow represents tetration and to calculate \\(2\uparrow\uparrow3=2^{2^2}\\) he would input \\(2^2\times3^3\times5^3=13500\\).  Both of these test cases worked splendidly.

Unfortunately, when he tried to calculate \\(5\uparrow\uparrow3=5^{5^5}\\) (by inputting \\(2^5\times3^3\times5^3=108000\\)), it didn't evaluate immediately.
You find him sitting in front of his computer waiting for it to finish.
You need to convince him that this is a stupid idea.
If one step is when you successfully multiply the current value n by a fraction f, find the total number of steps the program will take before it successfully calculates \\(5\uparrow\uparrow3\\).
(The two test cases, for example, should take 10084 steps and 914 steps, respectively.)

Output: If x is the total number of steps, input the integer formed by taking every 115th digit of x, starting from the leftmost digit.

##Solution

First of all, for this problem, our standard FRACTRAN interpreter won't do (at least, not in Java).  The numbers involved will quickly go over the Integer limit, the Long limit, and even the BigInteger limit.  Instead of working with numbers directly for this one, we'll instead want to work with numbers as an array of prime exponents.  We don't actually need to know, for example, what 5^1000 is--we only care that the exponent of 5 is 1000.  You can see the code I used in the **Appendix** at the bottom of this write-up under **First Implementation of eval**. (It makes use of three other methods: a prime factorization algorithm, a conversion from an arrayList of prime factors into an array, and a method to print the current value of N, all three of which appear directly below it in the **Appendix**).

This code works fine when testing the two testcases given in the problem.  However, it won't work for the actual case the problem asks you to solve.  Not only will it never finish in your lifetime (if you want to estimate how long it will take, go ahead; I'm too lazy), even if it could finish it would exceed the Integer Cap on the powers of the exponents.  That's right--not only is the output itself too big to calculate, the exponents themselves go beyond the integer cap in Java.  Fortunately, BigInteger will be able to handle it.  We'll convert all the storage of exponents, as well as the storage for number of steps, into BigIntegers, and continue with the optimization.

The next thing to worry about is how to make this program actually able to execute in a reasonable amount of time. Now, you might decide to print out the fractions, look at the execution, and puzzle over it with some pencil and paper for a few hours, or even days (I know there were some people who did this). However, if you're like me, you can't be bothered to figure out exactly what the code is doing or why it works.  Instead, my solution was to see where the program got stuck looping through the same few fractions for hundreds or thousands of times.  For each of these loops that I found, I would figure out what it was doing and how many steps it took.  Then, I would hardcode into the interpreter that whenever it was about to start one of the loops, instead it would skip right to the end result and add the appropriate number of steps.  You can see the code I used for this under **Second Implementation of eval** in the **Appendix** (as well as the updated version of the "print" method below it).

Here are the major loops I found:
* **Fractions 6-11:** Multiplication: 7s += 2s \* 5s. 5s are reduced to 0.
 * Steps taken: (4 \* Number of 2s \* Number of 5s) + (2 \* Number of 5s)
* **Fractions 13-14:** Move the 7s to the 5s.
 * Steps taken: (2 \* Number of 7s)
* **Fractions 22-28:** Division with remainder: 19s = 17s / 7s and 23s = Remainder.  17s are reduced to 0, and 7s are reduced by the remainder.
 * Steps taken: (4 \* Number of 7s + 2)\*(17s/7s) + (2 \* Remainder)
* **Fractions 38-39:** Move the 19s to the 17s.
 * Steps taken: (2 \* Number of 19s)
* **Fractions 44-50:** Multiplication: 19s += 7s \* 17s. 17s are reduced to 0.
 * Steps taken: (4 \* Number of 7s \* Number of 17s) + (3 \* Number of 17s)
* **Fractions 55-56:** Move the 19s to the 17s.
 * Steps taken: (2 \* Number of 19s)

After implementing all of these time-savers, the program (which you can see in the **Appendix** under **Final Implementation of eval**) is finally able to execute fast enough to calculate the deisred number as well as the number of steps taken, which is over 2000 digits long.
The total number of steps can be found at the bottom of the **Appendix**.
The flag, which is every 115th digit, is **15056605647060484408**.

Author's note: due to some flaw in how I calculated it, the optimization for the 6-11 loop had to be implemented slightly differently in the code than the other optimizations.  I'll be sure to figure out what's causing it, but rest assured that it doesn't affect the calculation or answer at all, only the execution of the program.

##Appendix
###List of Fractions in Java array format:
    {{263,22725},{2055,263},{223,909},{2061,1115},{227,303},{687,1135},{17941,458},{229,233},{239,229},{482,2629},{239,241},{229,1195},{251,239},{1285,1757},{251,257},{31,251},{2233,5239},{403,29},{22099,403},{481,451},{41,37},{47,41},{989,5593},{47,43},{67,329},{1121,47},{371,1357},{59,53},{47,59},{61,469},{67,61},{73,67},{71,219},{73,71},{83,73},{237,1909},{83,79},{97,83},{1513,1843},{97,89},{101,97},{917,23153},{1781,131},{6811,1781},{31141,1057},{151,149},{163,151},{1099,1793},{163,157},{139,163},{151,2363},{173,139},{2171,8477},{1211,167},{181,1211},{3043,3439},{181,179},{193,181},{685117,579},{193,191},{199,193},{591,41989},{199,197},{303,199},{9919,321},{107,109},{113,107},{381,791},{113,127},{101,1469},{321,103},{103,3},{1,2},{1,13},{1,101}}


###First Implemntation of eval:

    public int[] vectorEval(int i, int[][] fractions) {
    		ArrayList<Integer> input = primeFactors(i);
    		ArrayList<ArrayList<Integer>> nums = new ArrayList<ArrayList<Integer>>();
    		ArrayList<ArrayList<Integer>> dens = new ArrayList<ArrayList<Integer>>();

    		for (int[] frac : fractions) {
    			nums.add(primeFactors(frac[0]));
    			dens.add(primeFactors(frac[1]));
    		}

    		int max = 0;
    		for (ArrayList<Integer> in : nums)
    			for (int j : in)
    				max = Math.max(max, j);
    		for (ArrayList<Integer> in : dens)
    			for (int j : in)
    				max = Math.max(max, j);

    		int[][] numsA = toArray(max, nums, dens)[0];
    		int[][] densA = toArray(max, nums, dens)[1];

    		int[] inp = new int[max + 1];
    		for (int n : input)
    			inp[n]++;

    		int steps=0;
    		int counter = 0;

    		while (counter < nums.size()) {
    			int[] num = numsA[counter];
    			int[] den = densA[counter];
    			boolean good = true;
    			for (int n = 0; n < den.length; n++)
    				if (den[n] > 0) {
    					if (inp[n] < den[n]) {
    						good = false;
    						break;
    					}
    				}

    			if (good) {
    				steps++;
    				print(inp);
    				for (int n = 0; n < inp.length; n++) {
    					inp[n] -= den[n];
    					inp[n] += num[n];
    				}
    				counter = 0;
    				print(inp);

    			} else
    				counter++;
    		}
    		System.out.println("STEPS: "+steps);
    		return inp;
    	}
### Prime Factorization Algorithm:

	public static ArrayList<Integer> primeFactors(int number) {
		int n = number;
		ArrayList<Integer> factors = new ArrayList<Integer>();
		for (int i = 2; i <= n; i++) {
			while (n % i == 0) {
				factors.add(i);
				n /= i;
			}
		}
		return factors;
	}
### Convert the ArrayLists of fractions	into a single array for ease of access:
	public static int[][][] toArray(int max,ArrayList<ArrayList<Integer>> nums,ArrayList<ArrayList<Integer>> dens) {

		int[][][] array = new int[2][nums.size()][max + 1];
		for (int i = 0; i < nums.size(); i++) {
			for (int j : nums.get(i))
				array[0][i][j]++;
		}
		for (int i = 0; i < dens.size(); i++) {
			for (int j : dens.get(i))
				array[1][i][j]++;
		}
		return array;
	}

###Print the current array of exponents (Integer)
    public static void print(int[] i) {
    		for (int n = 0; n < i.length; n++) {
    			if (i[n] > 0) {
    				System.out.print(n + "^" + i[n] + "  ");
    			}
    		}
    		System.out.println();
    	}

###Second Implementation of eval
	public BigInteger[] vectorEval(int i, int[][] fractions) {
		ArrayList<Integer> input = primeFactors(i);
		ArrayList<ArrayList<Integer>> nums = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> dens = new ArrayList<ArrayList<Integer>>();
		BigInteger steps = BigInteger.valueOf(0L);

		for (int[] frac : fractions) {
			nums.add(primeFactors(frac[0]));
			dens.add(primeFactors(frac[1]));
		}

		int max = 0;
		for (ArrayList<Integer> in : nums)
			for (int j : in)
				max = Math.max(max, j);
		for (ArrayList<Integer> in : dens)
			for (int j : in)
				max = Math.max(max, j);

		int[][] numsA = toArray(max, nums, dens)[0];
		int[][] densA = toArray(max, nums, dens)[1];
		BigInteger[] inp=new BigInteger[max+1];
		for (int j=0;j<inp.length;j++)
			inp[j]=new BigInteger("0");
		for (int n : input)
			inp[n]=inp[n].add(BigInteger.ONE);


		int counter = 0;

		while (counter < nums.size()) {
			int[] num = numsA[counter];
			int[] den = densA[counter];
			boolean good = true;
			for (int n = 0; n < den.length; n++)
				if (den[n] > 0) {
					if (inp[n].compareTo(BigInteger.valueOf((long)den[n]))<0) {
						good = false;
						break;
					}
				}

			if (good) {
				System.out.print(counter+": ");
				print(inp);
					steps=steps.add(BigInteger.ONE);

					for (int n = 0; n < inp.length; n++) {

						inp[n]=inp[n].subtract(BigInteger.valueOf((long)den[n]));
						inp[n]=inp[n].add(BigInteger.valueOf((long)num[n]));
					}
					counter = 0;


			} else
				counter++;

		}
		print (inp);
		System.out.println(steps.toString()+" steps");
		return inp;

	}

###Print the current array of exponents (BigInteger)
    public static void print(BigInteger[] i) {
		for (int n = 0; n < i.length; n++) {
			if (i[n].compareTo(BigInteger.ZERO)>0) {
				System.out.print(n + "^" + i[n] + "  ");
			}
		}
		System.out.println();
	}

###Final implementation of eval:

	public BigInteger[] vectorEval(int i, int[][] fractions) {
		ArrayList<Integer> input = primeFactors(i);
		ArrayList<ArrayList<Integer>> nums = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> dens = new ArrayList<ArrayList<Integer>>();
		BigInteger steps = BigInteger.valueOf(0L);

		for (int[] frac : fractions) {
			nums.add(primeFactors(frac[0]));
			dens.add(primeFactors(frac[1]));
		}

		int max = 0;
		for (ArrayList<Integer> in : nums)
			for (int j : in)
				max = Math.max(max, j);
		for (ArrayList<Integer> in : dens)
			for (int j : in)
				max = Math.max(max, j);

		int[][] numsA = toArray(max, nums, dens)[0];
		int[][] densA = toArray(max, nums, dens)[1];

		BigInteger[] inp=new BigInteger[max+1];
		for (int j=0;j<inp.length;j++)
			inp[j]=new BigInteger("0");
		for (int n : input)
			inp[n]=inp[n].add(BigInteger.ONE);


		int counter = 0;

		while (counter < nums.size()) {
			int[] num = numsA[counter];
			int[] den = densA[counter];
			boolean good = true;
			for (int n = 0; n < den.length; n++)
				if (den[n] > 0) {
					if (inp[n].compareTo(BigInteger.valueOf((long)den[n]))<0) {
						good = false;
						break;
					}
				}

			if (good) {
				if (counter==6){
					steps=steps.add(new BigInteger("4").multiply(inp[2]).multiply(inp[5]).add(new BigInteger("2").multiply(inp[5])));
					inp[7]=inp[7].add(inp[2].multiply(inp[5]));
					inp[5]=new BigInteger("0");
					counter=0;
				}

				if (counter==13)
				{
					steps=steps.add(inp[7].add(inp[7]));
					inp[5]=inp[7];
					inp[7]=new BigInteger("0");
					counter=0;
				} else
				if (counter==38)
				{
					steps=steps.add(inp[19].add(inp[19]));
					inp[17]=inp[19];
					inp[19]=new BigInteger("0");
					counter=0;
				} else
				if (counter==22) {
					steps=steps.add(   (new BigInteger("4").multiply(inp[7]).add(new BigInteger("2"))).multiply(inp[17].divide(inp[7])).add(new BigInteger("2").multiply(inp[17].mod(inp[7]))));
					inp[19]=inp[17].divide(inp[7]);
					inp[23]=inp[17].mod(inp[7]);
					inp[7]=inp[7].subtract(inp[23]);
					inp[17]=new BigInteger("0");
					counter=0;
				} else
				if (counter==50)
				{
					steps=steps.add(new BigInteger("4").multiply(inp[7]).multiply(inp[17]).add(new BigInteger("3").multiply(inp[17])));
					inp[19]=inp[7].multiply(inp[17]);
					inp[17]=new BigInteger("0");
					counter=0;

				} else
				if (counter == 55) {
					inp[17] = inp[19];
					inp[19] = new BigInteger("0");
					steps=steps .add(inp[17].add(inp[17]));
					counter = 0;
				} else
				{
					steps=steps.add(BigInteger.ONE);

					for (int n = 0; n < inp.length; n++) {

						inp[n]=inp[n].subtract(BigInteger.valueOf((long)den[n]));
						inp[n]=inp[n].add(BigInteger.valueOf((long)num[n]));
					}
					counter = 0;

				}

			} else
				counter++;

		}
		System.out.println(steps.toString()+" steps");
		return inp;

	}
### Total Number of Steps:

	15288100783563820162851236477631716793584648391920754697116102313972164241940926424096475092157918802124691521246355697882873702309390455778307281175327892816703695174057565614714253282704380889807804269008808011861208987305182146019291944755577415722262050281573900115162860339832745753653135064094297352980419368560405672517395062966811599602052689555692345468091038793069534239425019470723452393894844214268770992816842652368250829802605989095273278229522626131673463870521842295492582989531381185050333950097090605842573162263835894900609962897744141385799524541142554880629765710696578842443570849231571209482787950341756116756044065861993772846791303287561519679589489596345181335504104300520605789052098705408682437274839637784605913033378416820480940059955845979915797300907985387990368711682648445460913301724266454878963644287002778717699756175775279228604155314133907528319937504597846942855600020948072014103616199629598392232517591799001073380007028433262044055518301658876015655861713395266131136411034278505927720675360143544435621472205436744421902484465783126297605561471918141499025369509303036039900819614971824298218995031169092697850622311996127265474380896353968757049920339500133625220233761455691676784647613212591763482811644873727850717807087576276598181412052642977603331127949611283459447391300979107472582466891502171272727669200970726624708089381951068990695366182122360791646410339858308785618254565295819635180825388843335269792855963961127456355581410185160422430304978518021076727454913488500382154815549675588117505230894072726677476315467215963765925340389096324982146301279490294557491947470266749370597237544868985892477192887934111200070714004542640593464950613793364586882224558298747889464039638543660659168871396209735586045061053557998636794335800159969953873434795450118135901758295094608904948614862395351805565526025563989252563236386827053759484151795771044017069590439151215069793165787674780629388076000774408855961551448727651380032341913020638742776807301302725095295583254039474620309304012466447708436585305148392639406620248687442455177238711421901983314258041758062256033384982192683948997282604336563482813513914491844339459953388
