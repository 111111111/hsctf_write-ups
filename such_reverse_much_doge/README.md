# Such Reverse Much Doge

Type: Reversing

By Alok Tripathy

##Problem

Everybody knows the doge is an awesome programmer, but sometimes he can be a little silly.
For instance, he just forgot the password for a program he just made.
Help doge out by finding out the password in this file:

    import javax.swing.JButton;
    import javax.swing.JTextField;
    import javax.swing.JFrame;
    import java.awt.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    import javax.swing.JOptionPane;
    public class Reversing1 implements ActionListener {

        private String dogeDog = "";
        private JButton buttonDoge;
        private JTextField fieldDoge;
        private JFrame frameDoge;

        public Reversing1() {
            frameDoge = new JFrame("Reversing 1");
            frameDoge.setLayout(new FlowLayout());
            frameDoge.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            buttonDoge = new JButton("Submit");
            frameDoge.getContentPane().add(buttonDoge);

            fieldDoge = new JTextField(20);
            frameDoge.getContentPane().add(fieldDoge);

            buttonDoge.addActionListener(this);



            frameDoge.setSize(400, 90);
            frameDoge.setVisible(true);

        }

        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == buttonDoge) {
                dogeDog = fieldDoge.getText();
                if(dogeDog.length() != 13) {
                    JOptionPane.showMessageDialog(frameDoge, "Woops");
                } else {
                    if(dogeDogeDogeDogeDoge(dogeDog).equals("uihrhruidenfd")) {
                        JOptionPane.showMessageDialog(frameDoge, "woohoo, you got it!");
                    } else {
                        JOptionPane.showMessageDialog(frameDoge, "Woops");
                    }
                }
            }
        }

        public String dogeDogeDogeDogeDoge(String doge) {
            char[] suchChar = doge.toCharArray();
            char[] manyToReturn = new char[suchChar.length];
            for(int muchIndex = 0; muchIndex < suchChar.length; muchIndex++) {
                int soTemp = (int)suchChar[muchIndex];
                manyToReturn[muchIndex] = (char)(((soTemp & 11 | ~(0 >> 3)) & (soTemp ^ 1))) ;
            }

            return new String(manyToReturn);
        }

        public static void main(String[] args) {
            Reversing1 test=new Reversing1();
        }
    }

##Solution

This program takes an input string, changes it with an algorithm to something else, and then checks this new string against the scrambled version of the flag to see if it is correct.  To solve this, first we see that it changes each character in the string independently, so that means that each letter always is changed to the same new character regardless of its place in the string.  We can therefore use the code to figure out exactly which letter maps to which, and reverse this mapping without figuring out exactly what the code does.

I put this code in the main to do that:

    String code="uihrhruidenfd";
    String original="";
    for (int letter=0;letter<code.length();letter++)
    	for (char c=10; c<=150; c++)
    		if (test.dogeDogeDogeDogeDoge(String.valueOf(c)).charAt(0)==code.charAt(letter))
    			original+=c;
    System.out.println(original);

This will tell you that the original string, and therefore the flag, is **thisisthedoge**.

