# Hoboken Houses
___
Type: Algo

By: Ernest Chiu

##Problem

The town of Hoboken is surrounded by large, insurmountable walls. There is a house on each point with integer coordinates strictly inside the walls.
Keith the survey corps soldier is patrolling the walls when he wonders how many houses there are in Hoboken.

Given the path Keith takes, can you tell him how many houses there are?

Input Form

Input

Output:
The number of houses in Hoboken.

##Solution
tl:dr; Shoelace, Pick's, Big Numbers

By [Pick's theorem](http://en.wikipedia.org/wiki/Pick%27s_theorem), A = i + b/2 -1, where A is the area of an simple polygon with vertices on integer points, i is the number of interior points, and b is the number of boundary points. So i = A - b/2 + 1.

Using the [Shoelace Formula](http://en.wikipedia.org/wiki/Shoelace_formula), we can calculate the area of a simple polygon from the x- and y- coordinates of its vertices.
We can count the number of boundary points by summing the greatest common divisor of the x- and y- coordinates of path movements.

The only problem is that the numbers obtained are slightly too big for common formats. Use a programming language that can handle big numbers, like Microsoft Excel, and you end up with the answer 44181807272927213919.
