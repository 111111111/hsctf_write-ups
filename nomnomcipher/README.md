#NomNomCipher

Type: Crypto

By Aaron Berger

##Problem
Taxnwa (/ˈæzɨtə/ gr /əˈmɪeə/; ?–453), ncejbiunye icylvwpd zw ss Tmbtlt all Bht, nyl alj curmj oy mpp Hnuw mlbs 434 llmpp mts jmsta bv 453. Se phw synjvp hm xmp Havfiv Xuaikl, aocpn jrklxhsej njof mpp Ukhp Yciki rh alj Chovw Rbomc agk jyiz zyc Whrzme Xqnek mw ehx Iesnvi Jct.

##Solution
Searching the two numbers in the ciphertext, 434 and 453, should lead you to [the Wikipedia article on Attila the Hun](https://en.wikipedia.org/wiki/Attila). The plaintext is pretty clearly the first paragraph if this article, which is:

"Attila (/ˈætɨlə/ or /əˈtɪlə/; ?–453), frequently referred to as Attila the Hun, was the ruler of the Huns from 434 until his death in 453. He was leader of the Hunnic Empire, which stretched from the Ural River to the Rhine River and from the Danube River to the Baltic Sea."

You might guess that this is encrypted with one of the most basic ciphers, the Vigenere cipher.  You can decode it on a website such as [this one](http://www.mygeocachingprofile.com/codebreaker.vigenerecipher.aspx) by putting the encrypted text into the "Encrypted Text" box and the actual text of the article into the "Shift text using this key" box, which will leave you with a paragraph that contains the string "THEFLAGISATTILATHEHUNGRY" repeated multiple times, which tells you that the flag is therefore ATTILATHEHUNGRY.
