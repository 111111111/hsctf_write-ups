# yangwy
___
Type: Recon

By: Ernest Chiu

##Problem
My name is Ernest Chiu.
##Solution
tl:dr; aops

The first hit on google for 'yangwy blog' is a blog on artofproblemsolving.org.

The title of the only post on the blog is Field-tested Learning Assessment Guide, whose first letters spell FLAG.

At the bottom of the blog post, it says that the flag is CTFANDMATHCOMPETITIONSARETRICKYANDFUN.

**Flag:** CTFANDMATHCOMPETITIONSARETRICKYANDFUN

Fun fact: The first problem and last problem on the blogpost were the precursors of Sleepless Soccer Balls and Project Einstein, respectively.

