# Can You JPEG?

Type: Recon + Crypto

By Ben Edelman

#Problem

<img src=http://compete.hsctf.com/ciJsbu4VFk/ship.jpg>

#Solution

The problem consists of a corrupted JPEG image named "ship.jpg". It displays differently on different browsers and operating systems, and may be invisible entirely. This is because the header (the first part of the file that contains metadata and describes how the image is compressed) is incorrect in some way, rendering the actual image unviewable.

Knowing this, you could install one of various image recovery programs available online.

Or...you can solve it the fun way:
1. Note that the image is 1160 x 750 pixels (assuming this part of the headers is correct).
2. Note that the name of the image is "ship.jpg" (this must mean something)
3. Search "ship" on Google Images, restricting your search to images of size exactly 1160 x 750.
4. Look at the the #1 result, the only image that seems to actually depict a ship (the Enterprise from Star Trek):

<img src="http://img3.wikia.nocookie.net/__cb20120205211228/memoryalpha/en/images/4/40/Enterprise_firing_phaser_proximity_blast.jpg">

Alternatively, you can fix the headers of ship.jpg, find out that it looks like the Enterprise, and use Google's reverse image search to find the above image.

Now, let's look at the byte-wise differences between ship.jpg and the Enterprise image found online. Here we use the UNIX utility cmp, but a simple script written in one of many programming languages would also suffice.

```
$ cmp -b -l enterprise.jpg ship.jpg
   176  21 ^Q    22 ^R
215719 232 M-^Z 130 X
215720 337 M-_  165 u
215721 361 M-q  130 X
215722 137 _     56 .
215723 221 M-^Q 157 o
215724  16 ^N   166 v
215725  55 -    116 N
215726 266 M-6   56 .
215727 314 M-L  172 z
215728 311 M-I  141 a
215729  42 "     56 .
215730 211 M-^I 126 V
215731 332 M-Z  111 I
215732 171 y    152 j
215733 344 M-d  170 x
215734 131 Y    141 a
215735  43 #    105 E
215736 266 M-6  162 r
215737 212 M-^J 160 p
215738  31 ^Y   162 r
215739  20 ^P   151 i
215740 107 G    123 S
215741  40      145 e
215742 137 _    122 R
425949 324 M-T   56 .
425950  12 ^J    56 .
426113   1 ^A    56 .
426114 377 M-^?  56 .
426115 331 M-Y   56 .
```

Aha! The span of bytes from byte #215719 to #215742 is different between the two files, and almost every other byte is the same. Let's look at the differing string:

> XuX.ovN.za.VIjxaErpriSeR

The last part, "ErpriSeR" is suspicious because it looks similar to "Enterprise". This could be an artifact of a Vigenere cipher applied to a sequence of repeated characters (or one could guess that the first three words of the string decrypt to "the.key.is"). Sure enough, when we decrypt "XuX.ovN.za.VIjxaErpriSeR" assuming the Vigenere key is "enterprise", we find our solution: "the.key.is.defkhaaaaaaan" (ignoring capitalization).

**Flag:** defkhaaaaaaan
