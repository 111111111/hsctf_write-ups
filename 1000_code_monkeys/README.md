# 1000 Code Monkeys
By Jacob Edelman

Type: Algo
##Problem
You have succesfully tracked your target, an escaped tangerine named Keith, into a factory.
This particular factory produces volumes of Shakespeare's plays. They produce these books via one thousand monkeys, all plugging away at keyboards in their cubicles. You need to find the secrets of the tangerine, but there are huge amounts of gibberish Monkey text to sift through.

[Here's the file (read the first bit to get started)](http://compete.hsctf.com/ciJsbu4VFk/randomGenProb1.txt)
##Solution
Inside the file it is revealed to you that.
```Somewhere in the text of this file (after this part) is a reasonably long phrase in the english languge. It is the flag. Good Luck!```
Of course, you may have had to write a program to even see this part since the file was so large it crashed many text editors. The correction solution was to use your favorite programing language to search through the file for strings of english characters.

First we read in the file.

```python
data=open('randomGenProb1.txt').read()
```
Next we set our inital counter of how many consecutive letters or spaces we have found to 0.
```python
consecutive=0
```
Now we loop through the letters.

```python
for i in range(len(data)):
    c=data[i]
```
Next we check to see if the current letter is an english character or a space. If it is we incremement the consecutive letter/space counter by 1.
```python
    if c.lower() in 'abcdefghijklmnopqrstuvwxyz ':
       consecutive+=1
```
If it is not we first check how many consecutive letters/spaces we have had, if it is more then 11, just an arbitary value that is high enough to prevent you from having too many strings but low enough that the flag will almost certainly be at least that length, we print out the last consecutive letter counter amount of letters. Even if it is not more then seven we still set the consecutive letter/space counter to 0.
```python
    else:
        if consecutive>11:
            print data[i-consecutive:i]
        consecutive=0

```


After this is done we simply look through the printed out strings to see that the flag, ignoring the text that is at the start of the file.

**Flag:** good job on finding this and note that the next probs metal stick of unlocking is even harder to find
