# Perfekta Kodo

Type: Crypto

By: Ernest Chiu

##Problem

One time pad is a perfekta kodo. Keith has encrypted these ciphertexts with the sama key using the OTP.
No specialaj characters are in the plaintext, incluzive periods, commas, and semicolons.

Example:
message: perfektakodo
key: abcdefghijkl
ciphertext: PFTIIPZHSXNZ

Clarification: Spaces are only included to help deciphering, they are not included when applying the OTP.
To make it easier, we have now told you which cipher text contains the flag.

Find the flag.
##Solution

tl:dr; Esperanto, 2-time pad, Google Translate

Some of the words in the problem statement are in Esperanto, because all the plaintexts are in Esperanto. The shortest ciphertext is the flag, so we might as well truncate them to the same width.

The first ciphertext is a trap. The one-letter words in Esperanto are not 'a' and 'i'. (a and i are oni and mi, respectively).

The rest is simply guess-and-check, ad hoc approach, with help from Google Translate. 
Many letters do not exist in Esperanto, which helps a bit but not a lot (one of the words is xkcd). 
Some breaks make it easier than others (realizing that the five-letter words mostly are 'estas', Esperanto for is), 
but eventually you obtain that the flag says "la flago estas turno en malnova problemo", 
or in English: "the flag is a twist on an old problem".